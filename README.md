<div align="center"><h1><ins>DevOps Demo Project:</ins></h1></div> 
<div align="center"><h2>Complete CI/CD Pipeline with AWS EKS and AWS ECR</h2></div>

<div align="center">![1](img/devOps.png)</div>
<br>

<ins>DevOps Tools used in this project</ins>:<br>- Kubernetes<br>- AWS: EKS, ECR, and CloudFormation (via eksctl)<br>- Jenkins<br> - Apache: Groovy and Maven<br>- Docker<br>- Git<br>- GitLab<br>- IntelliJ IDEA<br>- Java (command for running the JAR)<br>- Windows PowerShell

<ins>Operating systems:</ins><br>- Linux<br>- Windows 11

<ins>Project Purpose:</ins> Deploy a complete Jenkins CI/CD pipeline with stages to automatically increment a Java application version, use Apache Maven to build a Java artifact, build and push a Docker image to a private AWS ECR Docker repository, and commit the updated Java application version to GitLab using Git.

This project provides a comprehensive walkthrough of writing a Jenkinsfile and accounts for the necessary prerequisites prior to additional Groovy code being written. A complete Jenkins CI/CD pipeline is successfully executed at the project’s conclusion.

<ins>Out of scope:</ins><br>- Containerized Jenkins installation<br>- Maven installation on Windows 11<br>- AWS CLI installation<br>- Git installation in Windows<br>- GitLab account creation<br>- AWS account creation<br>- AWS access key creation and configuration<br>- kubectl comand line tool installation in Windows<br>- Writing Java code

# Project Walkthrough 
I. [Preliminary Project Tasks](#clone)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; A) [Clone Java App Project Locally](#clone1)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; B) [Jenkins: Install Maven Build Tool](#javaPlugin)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; C) [Create Jenkinsfile: Specify Maven Build Tool](#firstJenkins)<br>
II. [AWS ECR Repository Creation & Setting Global Environment Variables Inside Jenkinsfile](#globalEnvECR)<br>
III. [CI Stages](#ciStages)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; A) [Stage: Increment Version](#incrementVersion)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; B) [Stage: Build Artifact for Java Maven App](#stageArtifact)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; C) [Stage: Build/Push Docker Image to AWS ECR](#stageDockerECR)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i) [Jenkins Prerequisites:](#prereqDocker)<br> 
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;a) [Credentials Binding Plugin](#credsBindingsPlugin)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;b) [Add ECR Credentials](#ecrCreds)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;c) [Confirm/Install Docker](#dockerInstall)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ii) [Create Dockerfile](#dockerfile)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;iii) [Update Jenkinsfile](#dockerJenkins)<br>
IV. [CD Stages](#cdStages)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;A) [Stage: Deploy New App Version to EKS Cluster](#stageAppEKS)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;i) [Prerequisites:](#prereqAppEKS)<br> 
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;a) [Jenkins Container:](#jenkinsContainer)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1) [Install kubectl](#kubectl)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2) [Install aws-iam-authenticator](#aia)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3) [Install envsubst](#envsubst)<br> 
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;b) [EKS Install & Cluster Creation](#eksInstallCreate)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;c) [Jenkins (cont'd):](#jenkinsContd)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1) [Configure kubeconfig File](#kubeconfig)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2) [Multibranch Pipeline Creation](#multibranch)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;3) [Add AWS Credentials](#awsCreds)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;d) [Kubernetes:](#k8s)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1) [Create Secret (AWS ECR Registry)](#secret)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;2) [Create Kubernetes Config Files](#manifests)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;ii) [Update Jenkinsfile](#eksExplain)<br>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;B) [Stage: Commit the Version Update](#commitVersion)<br>
V. [Jenkins CI/CD Pipeline and Troubleshooting](#pipelineTroubleshoot)<br>
VI. [Project Teardown](#projectTeardown)

## I. Preliminary Project Tasks <a name="clone"></a>
Assume the developers of a Java application make the GitLab code repository available here: https://gitlab.com/nanuchi/java-maven-app.git. As a precursor to starting the project, a few preliminary project tasks are required. First, the project repository must be cloned locally. Second, the Apache Maven build tool must be installed so Maven commands can be issued in the 'increment version' and 'build app' stages of the pipeline. Last, a Jenkinsfile will be created where Maven will be specified as a build tool in the pipeline. 

## A) Clone Java Application Locally <a name="clone1"></a>
First, the Java application project needs to be cloned to the local workstation.

1. <ins>Note</ins>: Although the installation of Git on Windows 11 is outside the scope of this project walkthrough, the Git installer can be found here: https://git-scm.com/download/win

    Using the following commands, open Windows PowerShell, change to the root of C:\ and clone the project locally. The project will be renamed as “complete-pipeline-ecr-eks.”

       cd ..
       git clone https://gitlab.com/nanuchi/java-maven-app.git complete-pipeline-ecr-eks

    The output below validates the project is successfully cloned.

    ![1cl](img/preliminary/clone/1.png)

2.	Open File Explorer and confirm the project folder “complete-pipeline-ecr-eks” exists with the project files inside.

    ![2cl](img/preliminary/clone/2.png)
 
3.	Open the IntelliJ IDEA code editor. At the Welcome screen, click <b>Open</b>.

    ![3cl](img/preliminary/clone/3.png)

4.	Browse to the project name at the root of C:\ and click <b>OK</b>.

       ![4cl](img/preliminary/clone/4.png)

5.	Click the <b>Trust Project</b> button to open the project folder.

       ![5cl](img/preliminary/clone/5.png)

6.	The folder hierarchy of the project will show in the left pane.

       ![6cl](img/preliminary/clone/6.png)

## B) Jenkins: Install Maven Build Tool <a name="javaPlugin"></a>
This section covers the installation of the Apache Maven build tool in Jenkins.

1.	Login to the Jenkins management UI and navigate to Dashboard > Manage Jenkins > Tools. Under Maven installations, click the <b>Add Maven</b> button.

    ![1mv](img/preliminary/maven/1.png)
 
2.	Name the Maven installation “Maven,” leave the Install automatically checkbox ticked, and ensure the latest version (3.9.4 of this writing) is selected. Click the <b>Save</b> button.

    ![2mv](img/preliminary/maven/2.png)
 
3.	Navigate back to Dashboard > Manage Jenkins > Tools and confirm the Maven installation is saved.

## C) Create Jenkinsfile: Specify Maven Build Tool <a name="firstJenkins"></a>
In this section, the configuration of the Jenkinsfile will start with a clean slate. In the Jenkinsfile, the pipeline will be declared on any available agent and the Maven build tool will be invoked for use.

1.	Restore the IntelliJ IDEA code editor. Delete the script.groovy file. Open the existing Jenkinsfile and clear the file contents.

    ![1ij](img/preliminary/initialJenkins/1.png)
 
2. Copy and paste the following Groovy code into the Jenkinsfile and save the file.

       #!/usr/bin/env groovy

       pipeline {
           agent any
           tools {
              maven 'Maven'
           }
       }

    #!/usr/bin/env groovy is a shebang that tells code editors that this file is written in the Groovy language. 

    <i>agent any</i> tells Jenkins to run the entire pipeline on any agent that is available.

    The tools section specifies that the Maven build tool, configured in section B above, will be used in the pipeline. The value ‘Maven’ is the same name provided for the Maven installation name. 

3.	Save the Jenkinsfile.

## II.	AWS ECR Repository Creation & Setting Global Environment Variables Inside Jenkinsfile<a name="globalEnvECR"></a>
Before writing additional Groovy code in Jenkinsfile, an AWS ECR private repository will be created. After creation, environment variables will be added to the Jenkinsfile for the AWS ECR repo URL and the Java image repo.

1.	Login to the AWS Management Console and enter the search term “ecr.” Click on the top search result (Elastic Container Registry) to navigate to the ECR management interface.

      ![1eg](img/ecr_globalEnv/1.png)

2.	Under Create a Repository, click the <b>Get Started</b> button

    ![2eg](img/ecr_globalEnv/2.png)

3.	On the Create a Repository screen, confirm the Visibility settings are set to Private. Enter java-maven-app as the name of the repository. Leave all other settings as default.
      
      ![3eg](img/ecr_globalEnv/3.png)

       Click the <b>Create repository</b> button.
      
      ![4eg](img/ecr_globalEnv/4.png)

4.	Confirm the private AWS ECR repository displays on the screen. Next to URI, click the icon to copy the URI link to the clipboard.

    ![5eg](img/ecr_globalEnv/5.png)

5.	Restore the Jenkinsfile in the IntelliJ IDEA code editor. Enter a carriage return below the tools code block. At the same indentation level at the tools block, create an environment code block. Create two environment variables: ECR_REPO_URL and IMAGE_REPO. These variables are the same, except IMAGE_REPO includes java-maven-app at the end (i.e. the exact URI copied in Step 4).  

        environment {
            ECR_REPO_URL = '903183430766.dkr.ecr.us-east-2.amazonaws.com'
            IMAGE_REPO = "${ECR_REPO_URL}/java-maven-app"
        }

6.	 Save the Jenkinsfile. These two environment variables can now be used in subsequent stages in the pipeline.

## III. CI Stages<a name="ciStages"></a>
With the pipeline declared and the Maven build tool and environment variables accounted for, the Continuous Integration (CI) stages of the Jenkins pipeline can now be written inside Jenkinsfile. The CI stages for this project include dynamically incrementing the Java application version, building the JAR file as an application artifact, and building and pushing the Docker image to AWS ECR. These three stages are discussed next.

## A) Stage: Increment Version<a name="incrementVersion"></a>
Since the Apache Maven build tool is installed on the Jenkins server from section I.B. above, a pipeline stage can now be written in Jenkinsfile to use Maven commands as a means of incrementing the Java application version.

1. If minimized, restore Jenkinsfile inside the IntelliJ IDEA code editor. Underneath the environment code block, copy and paste the below code to act as a template for the stages code block. Notice there are a few code blocks nested inside the stages code block. Before proceeding, ensure that all parentheses are closed within each nested code block. This is where the shebang at the beginning of the Jenkinsfile shines, as just by highlighting any given parenthesis, the editor will determine if any parentheses are missing or extraneous. 

        stages {
           stage('increment version') {
              steps {
                 script {
                 }
              }
           } 
        }

    The stages code block encompasses all stages in the pipeline. Each stage will be provided a name under their respective singular stage code block. Accordingly, the first stage nested inside the stages code block is provided a name of ‘increment version.’ The increment version stage title and subsequent stage titles will visually appear as columns in Jenkins when the pipeline is built towards the end of this project. 

    Under the 'increment version' stage block, notice two additional nested code blocks exist – one for steps and one for script. The script code block will be the focus of the code logic moving forward. 

2. The script code block begins by displaying output, notifying the user of what is about to occur (e.g. the Java application will be incremented): 

        echo 'incrementing app version...'

3. Next, the following Maven command keeps the major and minor version the same but increments the patch (incremental) version of the Java application. The change is then automatically written (committed) in between the version XML tags in the pom.xml file: 

        sh 'mvn build-helper:parse-version versions:set \
                -DnewVersion=\\\${parsedVersion.majorVersion}.\\\${parsedVersion.minorVersion}.\\\${parsedVersion.nextIncrementalVersion} \
                versions:commit'

4. Then, a variable “matcher” is set to use the built-in readFile function, where the updated pom.xml file is read. The pom.xml file searches for the version tags.

        def matcher = readFile('pom.xml') =~ '<version>(.+)</version>'

5. A variable “version” is set to examine the matcher variable as an array. The first element [0] is the XML tag, while the second element in the array [1] provides the version value being sought.                 

        def version = matcher[0][1]

    From the pom.xml file and for clarity, the XML tag (element 0) and the version circled in red (element 1) is shown below.

    ![1](img/ci_stages/incrementVersion/1.png)

6. Further, an environment variable is defined as a concatenation of the version variable defined in Step 5 and the Jenkins built-in BUILD_NUMBER variable. Accordingly, expect the IMAGE_NAME to be formatted as  1.1.1-1 for the first run of the pipeline.  

        env.IMAGE_NAME = "$version-$BUILD_NUMBER"

7. Last, a series of hashes is printed for formatting of output and the global environment variable IMAGE_REPO defined in Section II, step 5 is printed as output:

        echo "############ ${IMAGE_REPO}"

8. The Jenkinsfile up to this point, including the complete ‘increment version’ stage, should now look like this: 

        #!/usr/bin/env groovy

        pipeline {
            agent any
            tools {
                maven 'Maven'
            }
            environment {
                ECR_REPO_URL = '903183430766.dkr.ecr.us-east-2.amazonaws.com'
                IMAGE_REPO = "${ECR_REPO_URL}/java-maven-app"
            }
            stages {
                stage('increment version') {
                   steps {
                      script {
                          echo 'incrementing app version...'
                          sh 'mvn build-helper:parse-version versions:set \
                              -DnewVersion=\\\${parsedVersion.majorVersion}.\\\${parsedVersion.minorVersion}.\\\${parsedVersion.nextIncrementalVersion} \
                              versions:commit'
                          def matcher = readFile('pom.xml') =~ '<version>(.+)</version>'
                          def version = matcher[0][1]
                          env.IMAGE_NAME = "$version-$BUILD_NUMBER"
                          echo "############ ${IMAGE_REPO}"
                      }
                  }
               }
           }
        }

Save the Jenkinsfile.

## B) Stage: Build Artifact for Java Maven App <a name="stageArtifact"></a>
With the Maven build tool available in Jenkins and the 'increment version' stage completed, a second stage in Jenkinsfile can now be written. This stage will use a Maven command to build a JAR file.

<ins>Note:</ins> Installation of the Apache Maven command line tool locally on Windows 11 is outside the scope of this project walkthrough.<br>- The Maven command line tool can be downloaded from here: https://maven.apache.org/download.cgi. <br>- Installation information for Windows is found here: https://maven.apache.org/guides/getting-started/windows-prerequisites.html. 

If a terminal is launched inside the IntelliJ IDEA code editor and the command <b>mvn package</b> is run locally, the Java application will be compiled/packaged and a JAR file will be generated inside the target folder. However, rerunning the <b>mvn package</b> command keeps old remnants of previous builds which is not a desired outcome. Evidence of the repackaging of old remenants is shown in the output here:

![1ba](img/ci_stages/buildArtifact/1.png)
 
Notice the target folder now exists inside the project folder:<br>
![2ba](img/ci_stages/buildArtifact/2.png)

The <b>mvn clean package</b> command, on the other hand, removes the contents of the target directory and recompiles/repackages the application with a fresh JAR file inside the target directory.  Evidence the target folder is cleaned can be seen in the output of the terminal inside IntelliJ IDEA:

![3ba](img/ci_stages/buildArtifact/3.png)
 
Therefore, to ensure the Java application is recompiled/repackaged whenever the pipeline is run without remnants from previous builds, the 'build app' stage of the pipeline will use the <b>mvn clean package</b> command.

1. Open the Jenkinsfile in the IntelliJ IDEA code editor. Under the 'increment version' stage, use the same indentation level and create a new stage block called 'build app.'

        stage('build app’) {
            steps {
                script {
                }
            }
        } 

2. Inside the script block, display output to the user that the application is being built. Then, use the <b>mvn clean package</b> command. The complete 'build app' stage looks like this:

        stage('build app') {
            steps {
                script {
                    echo "building the application..."
                    sh 'mvn clean package'
                }
            }
        }

## C) Stage: Build/Push Docker Image to AWS ECR <a name="stageDockerECR"></a>

The third stage of the Jenkins pipeline will build the Java application from Dockerfile as a Docker image, login to the Docker registry (AWS ECR), and push the Docker image to AWS ECR.

## i) Jenkins Prerequisites: <a name="prereqDocker"></a>
Prior to writing the next stage of the pipeline in Jenkinsfile, three prerequisites must be satisfied. First, the Credentials Binding plugin in Jenkins must be installed so Jenkins is able to login to AWS ECR (the Docker registry). Second, Docker must be installed inside the Jenkins container so the pipeline can issue Docker commands. Last, a Dockerfile needs to be created as a basis for the Docker image to be built.

## a) Credentials Binding Plugin<a name="credsBindingsPlugin"></a>

1. Login to the Jenkins server UI and navigate to Dashboard > Manage Jenkins > Plugins. Click the Installed Plugins option in the left menu and search for “credentials.” If the Credentials Binding plugin shows as enabled, proceed to the next prerequisite. 
 
    ![1cbp](img/ci_stages/buildPushDocker/Prereq/credentials_binding/1.png)

    Otherwise, click the Available plugins menu option in the left menu, search for the Credentials Binding plugin, and enable the plugin.

    The Credentials Binding plugin allows the use of a withCredentials block of code as shown here: https://plugins.jenkins.io/credentials-binding/ 
 

## b) Add ECR Credentials<a name="ecrCreds"></a>
1. Login to the AWS Management Console and navigate to the ECR service page. Click on the repository name java-maven-app.

    ![1ecrc](img/ci_stages/buildPushDocker/Prereq/ecr_credentials/1.png)
   
2. Once inside the java-maven-app repository, click the <b>View push commands</b> button. 

    ![2ecrc](img/ci_stages/buildPushDocker/Prereq/ecr_credentials/2.png)

3. <ins>Note</ins>: The installation of the AWS CLI (awscli) is outside the scope of this project walkthrough. Installation instructions can be found here: https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html.

    Copy the circled section of the first command to the clipboard. As of this writing, the instructions found on the macOS/Linux tab will suffice for a Windows workstation.

    ![3ecrc](img/ci_stages/buildPushDocker/Prereq/ecr_credentials/3.png)

4. Open PowerShell and paste in the circled command from Step 3 to display the ECR password. (Note: regions may differ). Minimize the PowerShell window.

    ![4ecrc](img/ci_stages/buildPushDocker/Prereq/ecr_credentials/4.png)

5. Login to the Jenkins management UI and navigate to Dashboard > Manage Jenkins > Credentials. At the bottom of the screen, in the “Stores scoped to Jenkins,” click (global). 

    ![5ecrc](img/ci_stages/buildPushDocker/Prereq/ecr_credentials/5.png)
 
6. On the Global credentials (unrestricted) screen, click the <b>+ Add Credentials</b> button in the right hand corner. 

    ![6ecrc](img/ci_stages/buildPushDocker/Prereq/ecr_credentials/6.png)
 
7. On the New credentials screen, leave the Kind dropdown option as the default (Username and password). Specify the username as AWS. Restore the PowerShell window from Step 4 and copy and paste the long password string into the Password field below. Enter the ID and Description as “ecr-credentials.” Click the <b>Create</b> button.

    ![7ecrc](img/ci_stages/buildPushDocker/Prereq/ecr_credentials/7.png)
 
8. Verify the ecr-credentials shows up in the credentials list.

## c) Confirm/Install Docker<a name="dockerInstall"></a>

1. SSH into the Linux host server that is running the Jenkins container. Check whether Docker is installed on the Linux host by entering the <b>docker</b> command and pressing Enter. If Docker is installed, the output will return usage and a list of Docker commands. Note the Jenkins server is running on Ubuntu and only partial output is displayed below:

    ![1cd](img/ci_stages/buildPushDocker/Prereq/confirm_docker/1.png)
 
    If Docker is not installed and the host server is running Ubuntu or Debian Linux distributions, Docker can be installed with the following command:

        apt install docker.io

    If another distribution of Linux is used for the Jenkins server, reference the documentation for installation instructions: https://docs.docker.com/engine/install/ 

2. Next, check whether Docker commands are available on the Jenkins container. Run the <b>docker ps</b> command to determine the container ID of the Jenkins server:

    ![2cd](img/ci_stages/buildPushDocker/Prereq/confirm_docker/2.png)

3. Enter the Jenkins container in an interactive terminal as the jenkins user, using the bash shell:

        docker exec -it <container id> bash

    The terminal prompt changes to reflect being inside the Jenkins container:

    ![3cd](img/ci_stages/buildPushDocker/Prereq/confirm_docker/3.png)
 
4. As done on the host server, check whether Docker is available in the Jenkins container by issuing the <b>docker</b> command and pressing Enter:

    ![4cd](img/ci_stages/buildPushDocker/Prereq/confirm_docker/4.png)

5. As the <b>docker</b> command is not available in the Jenkins container, it is necessary to stop the existing Jenkins container and create a new one. The new Jenkins container will reattach the existing and persisting Jenkins_home data volume and mount the host server’s Docker runtime directory as a volume to the container.  

    Copy the container ID suffix (i.e. after the @ sign) to the clipboard and issue the <b>exit</b> command to escape the existing Jenkins container.

    Enter the following command to stop the container:
    
        docker stop <container_id>

    ![5cd](img/ci_stages/buildPushDocker/Prereq/confirm_docker/5.png)
 
6. Create a new Jenkins container in detached mode (-d), specify ports 8080 and 50000, attach the existing and persisting Jenkins_home data volume, and attach the host server’s Docker runtime directory with the following command:

        docker run -p 8080:8080 -p 50000:50000 -d \
        -v jenkins_home:/var/jenkins_home                 
        -v /var/run/docker.sock:/var/run/docker.sock jenkins/jenkins:lts            

    ![6cd](img/ci_stages/buildPushDocker/Prereq/confirm_docker/6.png) 

7. Run the <b>docker ps</b> command and confirm the new container exists:

    ![7cd](img/ci_stages/buildPushDocker/Prereq/confirm_docker/7.png)

8. Refresh the Jenkins UI and test login. Notice that using this method of recreating the container preserves all Jenkins data (e.g. plugins, tools, pipeline jobs, etc.). 

    ![8cd](img/ci_stages/buildPushDocker/Prereq/confirm_docker/8.png)

9. Enter the newly created Jenkins container as the root user:

        docker exec –it u0 <container id> bash

10. Once inside the new Jenkins container, enter the following command to download the most recent version of Docker from the official Docker site, set permissions, and proceed with the installation.

        curl https://get.docker.com/ > dockerinstall && chmod 777 dockerinstall && ./dockerinstall

11. Once the install finishes, set Read and Write permissions to Everyone (including the jenkins user).

        chmod 666 /var/run/docker.sock

12. Exit the container and login to the new container as the standard jenkins user. Test the docker command. Docker is now available inside the Jenkins container!
 
    ![9cd](img/ci_stages/buildPushDocker/Prereq/confirm_docker/9.png)

Now that the Jenkins pipeline can execute docker commands, the last prerequisite of creating the Dockerfile is covered next.
## ii) Create Dockerfile <a name="dockerfile"></a>
1. Restore the IntelliJ IDEA code editor and create a new file called Dockerfile.
2. Copy and paste the following code into the Dockerfile:

        FROM openjdk:8-jre-alpine
        EXPOSE 8080
        COPY ./target/java-maven-app-*.jar /usr/app/
        WORKDIR /usr/app
        CMD java -jar java-maven-app-*.jar

    The FROM statement specifies using a base image of Alpine Linux with Java 8 installed. Port 8080 is exposed, and the JAR file is copied from the Git repository to the /usr/app directory of the Alpine Linux container. The /usr/app path is the working directory, and finally, the java command runs the application. 
3. Save the Dockerfile. Now that all prerequisites are satisfied, the next stage of the pipeline can be written.
## iii) Update Jenkinsfile <a name="dockerJenkins"></a>

   1. Open the Jenkinsfile, and directly underneath the 'build app' stage, copy and paste the below code as a template for the 'build image' stage.

            stage('build image’) {
                steps {
                   script {
                    }
                }
            }
    
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;As with other stages up to this point, the focus of the code logic will be in the script block.

2. This stage begins with displaying output to the user that the Docker image is about to be built. 

        echo “building the docker image…”

3. Next, two of the prerequisites will be put to use: the Jenkins Credentials Binding Plugin and ECR Credentials stored in Jenkins. Copy and paste the withCredentials code block from the following link: https://plugins.jenkins.io/credentials-binding/. Copying and pasting will help avoid syntactical errors in the pipeline. Specify ecr-credentials as the Credentials ID. For brevity, shorten PASSWORD to PASS and USERNAME to USER. 

        withCredentials([usernamePassword(credentialsId: 'ecr-credentials', passwordVariable: 'PASS', usernameVariable: 'USER')]) {

        }

4. Recall installing Docker inside the Jenkins container as a prerequisite. Inside the withCredentials code block, a series of Docker commands will be issued. The first Docker command builds the Docker image, specifying the -t option for tag. IMAGE_REPO is a global environment variable that is created in Section II, Step 5 of this walkthrough and is the link to the ECR repository. IMAGE_NAME is a variable defined in the 'increment version' stage of the pipeline. Recall that the IMAGE_NAME variable is the application version separated by a hyphen followed by the Jenkins build number (e.g. 1.1.1-1).

        sh "docker build -t ${IMAGE_REPO}:${IMAGE_NAME} ."

5. The second Docker command logs into the Docker registry (AWS ECR). $USER and $PASS from the withCredentials code line is referenced, and the password is passed as standard input. The <b>docker login</b> command does not require a registry URL if Docker Hub is used; however, since AWS ECR is used as a private docker registry, the AWS ECR URL is required at the end of the docker login command. Recall that ECR_REPO_URL is also a global environment previously defined in Section II, Step 5 of this walkthrough.

        sh "echo $PASS | docker login -u $USER --password-stdin ${ECR_REPO_URL}"

6. The last Docker command inside the withCredentials block pushes the tagged image to AWS ECR.

        sh "docker push ${IMAGE_REPO}:${IMAGE_NAME}"

7. The complete 'build image' stage looks like this:

        stage('build image') {
            steps {
                script {
                    echo "building the docker image..."
                    withCredentials([usernamePassword(credentialsId: 'ecr-credentials', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
                        sh "docker build -t ${IMAGE_REPO}:${IMAGE_NAME} ."
                        sh "echo $PASS | docker login -u $USER --password-stdin ${ECR_REPO_URL}"
                        sh "docker push ${IMAGE_REPO}:${IMAGE_NAME}"
                    }
                }   
            }
        }

## IV. CD Stages <a name="cdStages"></a>
This project implements two Continuous Delivery/Continuous Deployment (CD) stages in the Jenkins pipeline: (1) deploying the Java application to the EKS cluster and (2) committing the version update to the GitLab code repository. The prerequisites for these stages and formulating the stages in Jenkinsfile are discussed in this section.

## A) Stage: Deploy New App Version to EKS Cluster<a name="stageAppEKS"></a>
The next stage in the Jenkins pipeline involves deploying the Java application to the AWS EKS cluster. Prior to writing the Jenkinsfile, the prerequisites will be addressed first.

## i) Prerequisites: <a name="prereqAppEKS"></a> 
There are several prerequisites that must be satisfied before the 'deploy' stage can be written in Jenkinsfile. First, the kubectl, aws-iam-athenticator, and envsubst command line tools must be installed inside the Jenkins container. Second, the AWS EKS cluster must be created. Third, a kubeconfig file will be configured inside the Jenkins container. Then, the multibranch pipeline will be created in the Jenkins management UI where AWS credentials will be added to the pipeline scope. Last, a Kubernetes Secret for the AWS ECR Registry will be created, as well as the Kubernetes manifests for deployment and service of the Java application. Each prerequisite will be discussed in detail next.
 
## a) Jenkins Container <a name="jenkinsContainer"></a>

This section outlines installing the kubectl, aws-iam-authenticator, and envsubst command line tools inside the Jenkins container.

## 1) Install kubectl <a name="kubectl"></a> 

1. SSH into the Linux server that hosts the Jenkins container. Run the <b>docker ps</b> command to display the Jenkins server’s container ID. As installation requires root privileges, login to the Jenkins container as the root user. Verify if kubectl is installed by issuing the <b>kubectl</b> command.

    ![1k](img/cd_stages/deployAppEKS/Prereq/jenkinsContainer/kubectl/1.png)
 
2. Since the <b>kubectl</b> command is not found, the kubectl command line tool will be installed next. The official Kubernetes documentation is used as the basis of the install: https://kubernetes.io/docs/tasks/tools/install-kubectl-linux/ 

    Copy and paste the curl command to download the latest version of the kubectl command line tool:

        curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/linux/amd64/kubectl"

    Output from the curl command should look similar to this:

    ![2k](img/cd_stages/deployAppEKS/Prereq/jenkinsContainer/kubectl/2.png)
 
3. Copy and paste the following command to install the kubectl command line tool. As the root user is currently logged into the Jenkins container, issue the command without sudo:

        install -o root -g root -m 0755 kubectl /usr/local/bin/kubectl

4. Logout of the Jenkins container, and log back in as the jenkins user. Verify that kubectl is available to the jenkins user by using the following command:

        kubectl version –client

    ![3k](img/cd_stages/deployAppEKS/Prereq/jenkinsContainer/kubectl/3.png)
 
The kubectl command line tool is now ready to be used in the Jenkins pipeline.

## 2) Install aws-iam-authenticator <a name="aia"></a>
1. Login to the Jenkins container as the root user and check whether the aws-iam-authenticator command line tool is already installed. 

    ![1iam](img/cd_stages/deployAppEKS/Prereq/jenkinsContainer/aws-iam-authenticator/1.png)
 
2. As the <b>aws-iam-authenticator</b> command is not found, it will be installed next. The official AWS documentation is used as the basis of the install: https://docs.aws.amazon.com/eks/latest/userguide/install-aws-iam-authenticator.html 

    Copy and paste the following command to download the latest version of the aws-iam-authenticator command line tool. Note the command below is for AMD64 CPU architecture; if the Jenkins server is based on arm64 architecture, reference the link above and copy and paste that command instead.

        curl -Lo aws-iam-authenticator https://github.com/kubernetes-sigs/aws-iam-authenticator/releases/download/v0.5.9/aws-iam-authenticator_0.5.9_linux_amd64

3. Modify permissions so the binary can be executed:

        chmod +x ./aws-iam-authenticator

4. Move the binary to the /usr/local/bin path so that the jenkins user can execute the command:

        mv ./aws-iam-authenticator /usr/local/bin

5. Log out of the Jenkins container and log back in as the jenkins user. Verify the aws-iam-authenticator command line tool is available for use in the pipeline:

        aws-iam-authenticator version

    If successfully installed, the command will display the version.

    ![2iam](img/cd_stages/deployAppEKS/Prereq/jenkinsContainer/aws-iam-authenticator/2.png)
 
The aws-iam-authenticator command line tool is now ready to be used in the pipeline.


## 3) Install envsubst <a name="envsubst"></a>
For this project's use case, the envsubst command line tool will be used to substitute the variables inside the Kubernetes YAML files with variables set in the Jenkinsfile. Further information about this environment variable substitution, as well as details regarding the Kubernetes YAML files, will be explained in later sections. 
1. Login to the Jenkins container as the root user. Run the following command to update the apt-get software package list.

        apt-get update

2. Run the following command to download the gettext-base package, which includes the envsubst command:

        apt-get install gettext-base

    The output displayed on the screen will look like this:

    ![1env](img/cd_stages/deployAppEKS/Prereq/jenkinsContainer/envsubst/1.png)
 
3. Test the envsubst command by entering it into the command prompt. The command will not render any output but is now available as “command not found” is not displayed. Return to the bash shell by pressing Ctrl+C.

    ![2env](img/cd_stages/deployAppEKS/Prereq/jenkinsContainer/envsubst/2.png)
 
The envsubst prerequisite is complete. Exit out of the Jenkins container.
## b) EKS Install & Cluster Creation <a name="eksInstallCreate"></a>
The eksctl command line tool is used for the creation and management of AWS EKS Kubernetes clusters. This section walks through the installation of eksctl and the creation of an EKS cluster via command line.
1. Open PowerShell and issue the following command to ensure AWS credentials are already configured for programmatic access.

        aws configure list

    ![1eksi](img/cd_stages/deployAppEKS/Prereq/eksInstall_Cluster/1.png)
 
    Once AWS programmatic access is verified, minimize PowerShell.
2. The following link is used for the basis of the eksctl command line tool install, with installation instructions found in the middle of the page: https://eksctl.io/installation/

    On Windows 11, select the appropriate architecture type that matches that of the local machine. This project walkthrough assumes the AMD64/x86_64 option. Regardless of option selected, a zip folder will download to the user’s Downloads folder. 

    ![2eksi](img/cd_stages/deployAppEKS/Prereq/eksInstall_Cluster/2.png)
 
3.  Open File Explorer and navigate to the Downloads folder. Right-click on eksctl_Windows_amd64 and click Extract All…

    ![3eksi](img/cd_stages/deployAppEKS/Prereq/eksInstall_Cluster/3.png)
 
4. Click the <b>Extract</b> button to unzip the folder to the same location.

    ![4eksi](img/cd_stages/deployAppEKS/Prereq/eksInstall_Cluster/4.png)
 
 5. The eksctl executable will now show in File Explorer. 

    ![5eksi](img/cd_stages/deployAppEKS/Prereq/eksInstall_Cluster/5.png)
 
6. Although the eksctl installation is complete, the use of the command can only be accessed from within the folder it resides. The command can be made globally available by adding its command execution location to the $PATH environment variable, but for simplicity, the command will be executed from the Downloads location. Accordingly, close out of File Explorer and restore PowerShell. Enter the following command to change to the folder directory where the eksctl command resides:

        cd ".\Downloads\eksctl_Windows_amd64\"

7. Run the following command in PowerShell to create an EKS cluster named “demo-cluster” in the US-East-2 region with EKS version 1.27. This command also provides the name “demo-nodes” to the node group and creates 2 nodes with t2.micro instance sizes. This command also specifies a minimum node count of 1 and a max of 3.

        .\eksctl create cluster --name demo-cluster --version 1.27 --region us-east-2 --nodegroup-name demo-nodes --node-type t2.micro --nodes 2 --nodes-min 1  --nodes-max 3

    Some of the options from this command are highlighted in red in the screenshot below. Notice that AWS CloudFormation is used behind the scenes. In the background, CloudFormation will automatically create one private network and one public network for each of the 3 subnets in the US-East-2 region; required IAM roles for the EKS Cluster will also be created.

    ![6eksi](img/cd_stages/deployAppEKS/Prereq/eksInstall_Cluster/6.png)
 
    This process takes roughly 15-30 minutes to complete and will display the following output when ready:

    ![7eksi](img/cd_stages/deployAppEKS/Prereq/eksInstall_Cluster/7.png)
 
8. Login to the AWS management console and search for the EKS service. On the EKS management page, confirm the demo-cluster exists. Minimize the AWS Management console.

    ![8eksi](img/cd_stages/deployAppEKS/Prereq/eksInstall_Cluster/8.png)

9. <ins>Note:</ins> While this project walkthrough covers the installation of the kubectl command line tool inside the Jenkins container, installation of kubectl inside Windows is out of scope. Refer to the following link for Windows instructions: https://kubernetes.io/docs/tasks/tools/install-kubectl-windows/

    In PowerShell, enter the following command to confirm the connection to the AWS EKS cluster:

        kubectl get node

    ![9eksi](img/cd_stages/deployAppEKS/Prereq/eksInstall_Cluster/9.png)
 
    <ins>Note:</ins> The eksctl command executed earlier automatically configures the local kubeconfig file located at "C:\Users\username\.kube\config" and makes this connection possible without additional administrative intervention.

## c) Jenkins (cont'd): <a name="jenkinsContd"></a>
The following section addresses additional prerequisites associated with the Jenkins container and the Jenkins management UI. First, the kubeconfig file will be configured inside the Jenkins container. Then, a multibranch pipeline will be created in the Jenkins management UI. Last, AWS credentials will be stored inside Jenkins. These additional prerequisites are addressed next.

## 1) Configure kubeconfig File <a name="kubeconfig"></a>
1. SSH into the Linux host server where the Jenkins container resides. As a text editor is not installed inside the Jenkins container, the kubeconfig file will be created on the Linux host.

    On the Linux host, create a new file called config and open it in the VIM editor.

    ![1kc](img/cd_stages/deployAppEKS/Prereq/kubeconfig/1.png)
 
2. Press the “i” key to enter Insert Mode. Minimize the PowerShell window.
3. <ins>Note:</ins> The remainder of this section references official AWS documentation for creating the kubeconfig file located here: https://docs.aws.amazon.com/eks/latest/userguide/create-kubeconfig.html

    Scroll down to the middle of the page, confirm the AWS CLI tab is selected, and copy the YAML code to the clipboard. Restore the PowerShell window and paste the YAML code into the config file:

        #!/bin/bash
        read -r -d '' KUBECONFIG <<EOF
        apiVersion: v1
        clusters:
        - cluster:
            certificate-authority-data: $certificate_data
            server: $cluster_endpoint
          name: arn:aws:eks:$region_code:$account_id:cluster/$cluster_name
        contexts:
        - context:
            cluster: arn:aws:eks:$region_code:$account_id:cluster/$cluster_name
            user: arn:aws:eks:$region_code:$account_id:cluster/$cluster_name
          name: arn:aws:eks:$region_code:$account_id:cluster/$cluster_name
        current-context: arn:aws:eks:$region_code:$account_id:cluster/$cluster_name
        kind: Config
        preferences: {}
        users:
        - name: arn:aws:eks:$region_code:$account_id:cluster/$cluster_name
          user:
            exec:
              apiVersion: client.authentication.k8s.io/v1beta1
              command: aws
              args:
                - --region
                - $region_code
                - eks
                - get-token
                - --cluster-name
                - $cluster_name
                # - --role
                # - "arn:aws:iam::$account_id:role/my-role"
              # env:
                # - name: "AWS_PROFILE"
                #   value: "aws-profile"
        EOF
        echo "${KUBECONFIG}" > ~/.kube/config

4. <ins>Config file cleanup:</ins> Scroll up to the beginning of the file and delete the following two lines:

        #!/bin/bash
        read -r -d '' KUBECONFIG <<EOF

    Scroll to the bottom of the file and delete the last 7 lines:

                # - --role
                # - "arn:aws:iam::$account_id:role/my-role"
            # env:
                # - name: "AWS_PROFILE"
                #   value: "aws-profile"
        EOF
        echo "${KUBECONFIG}" > ~/.kube/config

5. Make the following changes to the config file:<br>- Enter kubernetes as the value for the Cluster Name and Cluster Context<br>- Enter aws as the value for User Context, Context Name, Current Context, and User Name<br>- Change the command entry to aws-iam-authenticator

    These changes are summarized below:

        apiVersion: v1
        clusters:
        - cluster:
            certificate-authority-data: $certificate_data
            server: $cluster_endpoint
          name: kubernetes
        contexts:  
        - context:
            cluster: kubernetes
            user: aws
          name: aws
        current-context: aws
        kind: Config
        preferences: {}
        users:
        - name: aws
          user:
            exec:
                apiVersion: client.authentication.k8s.io/v1beta1
                command: aws-iam-authenticator
                args:
                    - --region
                    - $region_code
                    - eks
                    - get-token
                    - --cluster-name
                    - $cluster


6. Restore the AWS Management Console and navigate to the EKS screen. Click on the demo-cluster link.

    ![2kc](img/cd_stages/deployAppEKS/Prereq/kubeconfig/2.png)
 
    On the Overview Tab, click the icon to copy the API server endpoint link to the clipboard. Minimize the AWS Management Console.

    ![3kc](img/cd_stages/deployAppEKS/Prereq/kubeconfig/3.png)
 
7. In PowerShell, restore the SSH session with the config file and paste the link from the clipboard as the value for the server. 

    ![4kc](img/cd_stages/deployAppEKS/Prereq/kubeconfig/4.png)
 
    Minimize the PowerShell window.

8. Restore the AWS Management Console. In the Overview Tab, copy the Certificate authority data to the clipboard.

    ![5kc](img/cd_stages/deployAppEKS/Prereq/kubeconfig/5.png)
 
    Minimize the AWS Management Console.

9. In PowerShell, restore the SSH session with the config file and paste the contents of the clipboard as the value for certificate-authority-data:

    ![6kc](img/cd_stages/deployAppEKS/Prereq/kubeconfig/6.png)

10. Change the region code; change the cluster name to “demo-cluster.”

    ![7kc](img/cd_stages/deployAppEKS/Prereq/kubeconfig/7.png)
 
11. Save and quit the file by entering the following command:

        :wq

12. Prior to copying the config file from the host server to the Jenkins container, a .kube directory must exist inside the jenkin’s home user directory. On the Linux host server, enter the <b>docker ps</b> command to view the Jenkins server Container ID. Copy the Container ID to the clipboard. Issue the following command to enter the Jenkins container as the jenkins user, replacing the container ID with the pasted contents from the clipboard:

        docker exec -it <container-id> bash

    Enter the <b>pwd</b> command to view the present working directory. Notice the current location is the root folder “/.” Enter the <b>cd ~</b> command to change to the jenkins user's home directory. Issue the <b>pwd</b> command again to determine the absolute path of the jenkins user's home directory. Create the .kube directory by issuing the <b>mkdir .kube</b> command. Exit the container and return to the host server’s command prompt. The below screenshot summarizes these steps:

    ![8kc](img/cd_stages/deployAppEKS/Prereq/kubeconfig/8.png)
 
13. From the Linux host, issue the following command to copy the config file from the Linux host to the .kube folder inside the jenkins user's home directory.

        docker cp config <container id>:/var/jenkins_home/.kube/

    Reenter the Jenkins container as the jenkins user by issuing the <b>docker exec -it</b> command. Verify the config file is copied to the /var/jenkins_home/.kube directory by issuing the <b>ls ~/.kube</b> command (recall that ~, or the home directory, is /var/jenkins_home as determined in Step 12. The screenshot below summarizes these steps:

    ![9kc](img/cd_stages/deployAppEKS/Prereq/kubeconfig/9.png)
 
14. The prerequisite of the kubeconfig file is now met. Exit the Jenkins container and close the SSH session.
## 2) Multibranch Pipeline Creation <a name="multibranch"></a>
1. Prior to adding AWS credentials to Jenkins, a skeleton multibranch pipeline will be created. Login to the Jenkins management UI. On the Dashboard, click +New Item from the left menu.

    ![1mb](img/cd_stages/deployAppEKS/Prereq/multibranch/1.png)
 
2. Specify the pipeline name as “complete-pipeline-ecr-eks” and select Multibranch Pipeline. Click <b>OK</b>. 

    ![2mb](img/cd_stages/deployAppEKS/Prereq/multibranch/2.png)
 
3. On the Configuration > General screen, enter the Display Name as “complete-pipeline-ecr-eks.” 

    ![3mb](img/cd_stages/deployAppEKS/Prereq/multibranch/3.png)
 
4. Additional configurations for the pipeline will be adjusted later, so for now, click the <b>Save</b> button at the bottom of the screen.

    ![4mb](img/cd_stages/deployAppEKS/Prereq/multibranch/4.png)
 
## 3) Add AWS Credentials <a name="awsCreds"></a>
1. In the Jenkins management UI, navigate to Dashboard > complete-pipeline-ecr-eks. On the complete-pipeline-ecr-eks screen, click Credentials from the left menu.

    ![1awsC](img/cd_stages/deployAppEKS/Prereq/awsCreds/1.png)


2. Under the “Stores scoped to complete-pipeline-ecr-eks” section, locate the complete-pipeline-ecr-eks store and click on the (global) domain.
    ![2awsC](img/cd_stages/deployAppEKS/Prereq/awsCreds/2.png)
 
3. Click the <b>+Add Credentials</b> button at the top right of the screen. The “New credentials” screen will now display.

    ![3awsC](img/cd_stages/deployAppEKS/Prereq/awsCreds/3.png)
 
4. <ins>Note:</ins> This project walkthrough assumes the engineer is in possession of the AWS access key for their account. If missing, create a new one from the AWS IAM management page and store it in a safe location.

    Minimize the Jenkins management UI window and open PowerShell. Enter the following command in PowerShell to view the AWS credentials for programmatic access:

        type ~/.aws/credentials

    The AWS Access Key ID and AWS Secret Access Key will display.

    ![4awsC](img/cd_stages/deployAppEKS/Prereq/awsCreds/4.png)
 
    Minimize the PowerShell window.
5. Restore the Jenkins management UI window. On the New Credentials page, specify “Secret text” from the Kind dropdown menu and specify the ID as jenkins_aws_access_key_id. Restore the PowerShell window and copy and paste the value for the aws_access_key_id into the Secret textbox. Click the <b>Create</b> button.

    ![5awsC](img/cd_stages/deployAppEKS/Prereq/awsCreds/5.png)
 
6. Repeat Steps 3-5, but this time, for the secret access key.

    ![6awsC](img/cd_stages/deployAppEKS/Prereq/awsCreds/6.png)
 
7. Confirm both Secret text credentials appear on the screen.

    ![7awsC](img/cd_stages/deployAppEKS/Prereq/awsCreds/7.png)

This prerequisite is now complete.

## d) Kubernetes: <a name="k8s"></a>
In this section, a Kubernetes Secret will be created for the AWS ECR Registry. Also, the Kubernetes deployment and service configuration files will be created for the Java application.
## 1) Create Secret (AWS ECR Registry) <a name="secret"></a>
1. Open PowerShell and paste (but hold on executing) the following command to create the Kubernetes Secret for the AWS ECR Registry.

        kubectl create secret docker-registry aws-registry-key –docker-server=<URL>  --docker-username=AWS –docker-password=<password>

    Where: <br>
--docker-server: This value is found by opening the AWS Management Console and navigating to Amazon ECR  > Repositories > java-maven-app > View push commands.

    ![1sec](img/cd_stages/deployAppEKS/Prereq/secret/1.png)
 
    On the macOS/Linux Tab for Step 1, copy and paste the circled, blurred out text as the value for the docker-server.

    ![2sec](img/cd_stages/deployAppEKS/Prereq/secret/2.png)
 

    --docker-user is AWS and is also the value of --username from the previous screenshot.

    --docker-password is the output from entering the following command:
        
        aws ecr get-login-password

    ![3sec](img/cd_stages/deployAppEKS/Prereq/secret/3.png)
 

2. If the command from Step 1 is executed successfully, the following output will display:

    ![4sec](img/cd_stages/deployAppEKS/Prereq/secret/4.png)
 
3. Confirm the secret exists by issuing the following command: 

        kubectl get secret

    ![5sec](img/cd_stages/deployAppEKS/Prereq/secret/5.png)
 
The Kubernetes secret for the AWS ECR repository is now complete.

## 2) Create Kubernetes Config Files <a name="manifests"></a>
1. Restore the IntelliJ IDEA code editor. Right-click the root directory of the project > New > Directory.

    ![1man](img/cd_stages/deployAppEKS/Prereq/manifests/1.png)
 
    Name the directory as kubernetes and create two YAML files inside by right-clicking the kubernetes folder > New > File. Names the two files deployment.yaml and service.yaml.

    ![2man](img/cd_stages/deployAppEKS/Prereq/manifests/2.png)
 
2. In deployment.yaml, copy and paste the code below as a template:

        apiVersion: apps/v1
        kind: Deployment
        metadata:
            name: java-maven-app
            labels:
              app: java-maven-app
        spec:
            replicas: 2
            selector:
              matchLabels:
                app: java-maven-app
            template:
              metadata:
                labels:
                  app: java-maven-app
              spec:
                imagePullSecrets:
                  - name: xxx
                containers:
                  - name: java-maven-app
                    image: xxx
                    imagePullPolicy: Always
                    ports:
                      - containerPort: 8080

3. Recall that creating the K8s Secret for the AWS ECR Registry is covered in Section d.1 above. Verify the name of the secret by entering the following command in Windows PowerShell:

        kubectl get secret

    The output should look like this. 

    ![3man](img/cd_stages/deployAppEKS/Prereq/manifests/3.png)
 
    Copy the name (aws-registry-key) to the clipboard. Restore the deployment.yaml file and paste the contents from the clipboard as the value for the imagePullSecrets name key.

    ![4man](img/cd_stages/deployAppEKS/Prereq/manifests/4.png)
 
4. From section II of this project walkthrough, recall that the global environment variables for ECR_REPO_URL and IMAGE_REPO are added to the Jenkinsfile. From Section III.A of this project walkthrough, where the 'increment version' stage of Jenkinsfile is written, also recall an environment variable IMAGE_NAME is created to function as a version tag (e.g. 1.1.1-1).

    ![5man](img/cd_stages/deployAppEKS/Prereq/manifests/5.png)
 
    With this in mind, specify $ IMAGE_REPO:$ IMAGE_NAME as the value for the container image:

    ![6man](img/cd_stages/deployAppEKS/Prereq/manifests/6.png)
 
    Save the deployment.yaml file.

5. In service.yaml, copy and paste the below code as a template:

        apiVersion: v1
        kind: Service
        metadata:
          name: java-maven-app
        spec:
          selector:
            app: java-maven-app
          ports:
            - protocol: TCP
              port: 80
              targetPort: 8080

    Notice the targetPort matches the exposed port in the Dockerfile.

    ![7sec](img/cd_stages/deployAppEKS/Prereq/manifests/7.png)
 
    Save the service.yaml file.

Now that all prerequisites have been satisfied, a Jenkins pipeline stage will be written to deploy the Java application to AWS EKS.
## ii) Update Jenkinsfile <a name="eksExplain"></a> 

1. Open the Jenkinsfile inside the IntelliJ IDEA code editor. Copy and paste the following template directly beneath the 'build image' stage. Name the stage 'deploy.'

        stage('deploy’) {
            steps {
                script {

                }
            }
        }

2. Inside the 'deploy' stage block, enter a couple carriage returns above the steps block. At the same indentation level as the steps block, create an environment block and define two environment variables: AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY. Recall that jenkins_aws_access_key_id and jenkins_aws_secret_access_key are stored in Jenkins as a prerequisite from section c.3.

    Set the jenkins_aws_access_key_id as the value for AWS_ACCESS_KEY_ID. Set the jenkins_aws_secret_access_key as the value for AWS_SECRET_ACCESS_KEY as shown below:

        environment {
            AWS_ACCESS_KEY_ID = credentials('jenkins_aws_access_key_id')
            AWS_SECRET_ACCESS_KEY = credentials('jenkins_aws_secret_access_key')
        }

3. In the left pane, open the deployment.yaml and service.yaml files that reside inside the kubernetes folder. Between the two files, notice that the value java-maven-app appears seven times. To generalize these Kubernetes configuration files to be used in other scenarios and ensure the java-maven-app value only needs be changed in one location (as opposed to 7x in the YAML files), return to the Jenkinsfile and create a third environment variable called APP_NAME in the environment block of the 'deploy' stage. Set the APP_NAME environment variable to java-maven-app.

        APP_NAME = 'java-maven-app'

4. Return to the deployment.yaml and service.yaml files and replace every instance of java-maven-app with the $APP_NAME environment variable. The service.yaml file is displayed below but the same method for the deployment.yaml applies.

    ![dECRj](img/cd_stages/deployAppEKS/jenkinsfile/1.png)

5. The script block will be the focus of the remaining code in this pipeline stage. First, display output to notify the user that the Java application is being deployed.

        echo 'deploying docker image...'

6. Recall from section IV.A.i.a.3 of this project walkthrough that installing the envsubst command line tool is a prerequisite to updating the Jenkinsfile. The next couple lines of code in the script block will make use of this prerequisite.

    The envsubst command line tool will substitute the placeholder values of $IMAGE_REPO, $IMAGE_NAME and $APP_NAME that appear in the Kubernetes configuration files with the associated variables set in the the Jenkinsfile. The values will be stored in a temporary file (e.g. kubernetes/deployment.yaml and kubernetes/service.yaml) and that temporary file will be piped/passed as a parameter for the filename (-) at the end of the <b>kubectl apply -f</b> command. To illustrate, the final two lines of code in the script block will look like this:

        sh 'envsubst < kubernetes/deployment.yaml | kubectl apply -f -'
        sh 'envsubst < kubernetes/service.yaml | kubectl apply -f -'

7. The final code for the 'deploy' stage is summarized below:

        stage('deploy') {
            environment {
                AWS_ACCESS_KEY_ID = credentials('jenkins_aws_access_key_id')
                AWS_SECRET_ACCESS_KEY = credentials('jenkins_aws_secret_access_key')
                APP_NAME = 'java-maven-app'
            }
            steps {
                script {
                    echo 'deploying docker image...'
                    sh 'envsubst < kubernetes/deployment.yaml | kubectl apply -f -'
                    sh 'envsubst < kubernetes/service.yaml | kubectl apply -f -'
                }
            }
        }

## B) Stage: Commit the Version Update <a name="commitVersion"></a>

The final stage of the Jenkins pipeline is committing the version update to the GitLab code repository. 

1. In the Jenkinsfile inside the IntelliJ IDEA code editor, copy and paste the following template directly beneath the 'deploy' stage. Name the stage 'commit version update.'

        stage('commit version update’) {
            steps {
                script {
                }
            }
        }

    Minimize the IntelliJ IDEA code editor.

2. GitLab credentials in Jenkins will be needed to authenticate with the GitLab code repository. Login to the Jenkins management UI and navigate to Dashboard > Manage Jenkins > Credentials.  

    ![dCom1](img/cd_stages/commitVersion/1.png)
 
    In this case, the gitlab-credentials already exist. However, if GitLab credentials need to be created, click the <b>+Add Credentials</b> button in the global scope and refer to the below screenshot for configuration. Specify the Kind dropdown as Username with password, provide the GitLab username, enter the GitLab password, and specify the ID as “gitlab-credentials.” The description is optional. Click the <b>Create</b> button.
 
    ![dCom2](img/cd_stages/commitVersion/2.png)

3. From Section III.C.i.a above, recall that the Credentials Binding plugin in Jenkins UI is already available for use. As a refresher, the Credentials Binding plugin allows the Jenkinsfile to make use of credentials stored in Jenkins. This plugin uses the withCredentials block.

    Inside the script block, copy and paste the withCredentials code block from the following link: https://plugins.jenkins.io/credentials-binding/. Copying and pasting will help avoid syntactical errors in the pipeline. Specify gitlab-credentials as the Credentials ID. For brevity, shorten PASSWORD to PASS and USERNAME to USER.

        withCredentials([usernamePassword(credentialsId: 'gitlab-credentials', usernameVariable: 'USER', passwordVariable: 'PASS')]) {
        }

4. The remainder of the code in this stage will be written inside the withCredentials block and will be a series of git commands. The first two git commands are global configurations that set the email associated with the GitLab repository and the username as jenkins. These two commands are only required for the first execution of the pipeline and can either be commented out or removed on subsequent pipeline executions. Replace the values in quotes with the correct email and username.

        sh 'git config user.email "your@email.com"'
        sh 'git config user.name "jenkins"'

5. If not already done, ensure a GitLab project exists for the pipeline. The next git command sets the remote source as the GitLab project URL. The $USER and $PASS variables set in the withCredentials block are passed to this line of code for authentication to the GitLab repository. 

        sh "git remote set-url origin https://${USER}:${PASS}@gitlab.com/jlomba09/ complete-pipeline-ecr-eks.git"

6. The next git command in the withCredentials block adds all changes from the working directory into the staging area. 

        git add .

7. Next, changes are committed from the staging area to the local repository with the following git command. The -m option automates specifying the commit message and eliminates the manual intervention of a saving a message in the code editor. The message indicates the version is updated by the pipeline. 

        sh 'git commit -m "ci: version bump"'

8. The last git command pushes the local code changes to the main branch of the remote GitLab code repository.

        sh 'git push origin HEAD:main'

9. A summary of the code written in the 'commit version update' stage is shown below:

        stage('commit version update') {
            steps {
                script {
                    withCredentials([usernamePassword(credentialsId: 'gitlab-credentials', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
                        sh 'git config user.email "your@email.com"'
                        sh 'git config user.name "jenkins"'
                        sh "git remote set-url origin https://${USER}:${PASS}@gitlab.com/username/complete-pipeline-ecr-eks.git"
                        sh 'git add .'
                        sh 'git commit -m "ci: version bump"'
                        sh 'git push origin HEAD:main'
                    }
                }
            }
        }

The Jenkinsfile is now complete. Please reference the final version via the Jenkinsfile above.

## V. Run Jenkins CI/CD Pipeline & Troubleshooting <a name="pipelineTroubleshoot"></a>

1. It is now time to finish configuring the Jenkins pipeline. Login to the Jenkins management UI. On the default screen that shows the list of jobs, click the “complete-pipeline-ecr-eks” job. Click Configure in the left pane. 

    ![1pt](img/pipeline_troubleshoot/1.png)

2. On the Configuration screen, locate the Branch Sources section. Click the Add source dropdown and select Git.

    ![2pt](img/pipeline_troubleshoot/2.png)

3. Under the Git section, copy and paste the GitLab URL into the textbox. Select the gitlab-credentials from the dropdown.

    ![3pt](img/pipeline_troubleshoot/3.png)

4. In the Behaviors section, click the Add dropdown and select “Filter by name (with regular expression).” 

    ![4pt](img/pipeline_troubleshoot/4.png)

5. In the Filter by name (with regular expression) textbox, specify main as the branch. Click <b>Save</b> at the bottom of the screen.

    ![5pt](img/pipeline_troubleshoot/5.png)

6. Return to the Dashboard and click inside the “complete-pipeline-ecr-eks” job. Click on the main branch and notice the pipeline will automatically start building.

    ![6pt](img/pipeline_troubleshoot/6.png)

7. <ins>Troubleshooting:</ins> The first error observed from the initial pipeline run pertains to AWS ECR credentials:

    ![7pt](img/pipeline_troubleshoot/7.png)

    After hovering over the 'build image' stage and clicking Logs, the following is displayed on the screen:

    ![8pt](img/pipeline_troubleshoot/8.png)
 
    To fix this error, navigate to Dashboard > Manage Jenkins > Credentials and locate the ecr-credentials entry. Open PowerShell and reenter the following command:

        aws ecr get-login-password

    Copy and paste the base64 value and update the password.

    The second error relates to the kubeconfig file on the Jenkins container. The screenshot below summarizes the changes made in the vim editor.

    ![9pt](img/pipeline_troubleshoot/9.png)

    The changes are copied back to the Jenkins container with the below command:

        docker cp config <container id>:/var/jenkins_home/.kube

    After these changes are made, the pipeline is rerun successfully. Select the build # and view the console output for verification.

    ![10pt](img/pipeline_troubleshoot/10.png) 

8. <ins>Validation:</ins> Validate the Docker image is pushed to the AWS ECR repository by logging into the AWS Management Console and navigating to the ECR management page. Click inside the java-maven-app registry and observe the pushed images:

    ![11pt](img/pipeline_troubleshoot/11.png)

    Open PowerShell and run the following commands to verify the Java pod is running and the service is created:

        kubectl get pod

    ![12pt](img/pipeline_troubleshoot/12.png)

        kubectl get svc

    ![13pt](img/pipeline_troubleshoot/13.png)
 
    Open the GitLab repo in a browser and confirm the commit message displays:

    ![14pt](img/pipeline_troubleshoot/14.png)

## VI. Project Teardown <a name="projectTeardown"></a>

Delete the EKS cluster by issuing the following command in PowerShell:

    .\eksctl delete cluster "demo-cluster"

This will take 10-15 minutes to complete.
 
![15pt](img/pipeline_troubleshoot/15.png)

In the AWS Management Console, navigate to the CloudFormation management page and confirm the stacks created by the eksctl command are deleting:

![16pt](img/pipeline_troubleshoot/16.png)

Confirm all stacks are deleted, and if necessary, manually delete any dependencies preventing deletion to complete successfully. When finished, the stack names above will no longer display.

![17pt](img/pipeline_troubleshoot/17.png)

![18pt](img/pipeline_troubleshoot/18.png)

AWS EKS cleanup is complete and no further charges will be incurred.
